import { Injectable, EventEmitter  } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppDataService {
  private subject = new Subject<any>();
  public  onError =  new EventEmitter<boolean>();;
  error = false;
  constructor() { }
  public sendMessage(val) {
      this.subject.next({error: val});
  }

  public getMessage(): Observable<any> {
      return this.subject.asObservable();
  }
  public setError(){
    this.error = true;
    this.onError.emit(this.error);
    return this.getError();
  }
  public removeError() {
    this.error = false;
    this.onError.emit(this.error);
    return this.getError();
  }
  public getError(){
    return this.error;
  }
}
