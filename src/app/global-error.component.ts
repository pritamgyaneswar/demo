import { AppDataService } from './app-data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  template: `
        <h2>An error occurred.</h2>
    `
})
export class GlobalErrorComponent implements OnInit {
  constructor(private appDataService : AppDataService) {
    this.appDataService.sendMessage(true);
  }

  ngOnInit() {
    // this.appDataService.sendMessage(true);
  }
}