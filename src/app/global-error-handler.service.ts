import { AppDataService } from './app-data.service';
import { Injectable, ErrorHandler, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {
    constructor(private injector: Injector, private appDataService: AppDataService) { 
        this.appDataService.sendMessage(true);
    }    

    handleError(error: any) {
      let router = this.injector.get(Router);
      console.log('URL: ' + router.url);  
      router.navigate(['/error']).then(_res =>{
      this.appDataService.setError();  
      })
    }
} 