import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AppDataService } from './app-data.service';
import { Component, OnInit, Injector } from '@angular/core';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit { 
	errorMsg: boolean;
	val: any;
	message: any;
	subscription: Subscription;
	errorUrl = false;
	constructor(private appDataService : AppDataService, private router: Router, 
		private activatedRoute: ActivatedRoute, private injector: Injector){
		// console.log(this.activatedRoute.snapshot.url)
		this.errorUrl = this.appDataService.getError();
		this.appDataService.onError.subscribe(data =>{
			console.log(data);
			this.errorUrl = data;
		});

		this.subscription = this.appDataService.getMessage().subscribe( message => this.message = message);
	}

	ngOnInit() {
		
		// console.log(this.appDataService.getMessage());
		// this.val = this.injector.get(Router);
		// // console.log(this.activatedRoute.pathFromRoot);
		// console.log('URL1: ' + JSON.stringify(this.val));
		// this.activatedRoute.paramMap.subscribe( params => {
		// 	console.log('snap', params);
		// })
		// if(location.pathname == '/error') {
		// 	this.errorMsg = true; 
		// } else {
		// 	this.errorMsg = false; 
		// }
	}

	ngOnDestroy() {
		// console.log(this.router.url);
        // unsubscribe to ensure no memory leaks
        // this.subscription.unsubscribe();
    }
}
    